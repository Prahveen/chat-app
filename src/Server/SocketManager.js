const io = require('./Server.js').io;
const { VERIFY_USER, USER_CONNECTED, USER_DISCONNECTED,
  LOGOUT, COMMUNITY_CHAT, MESSAGE_RECIEVED, MESSAGE_SENT,
  TYPING, PRIVATE_MESSAGE, } = require('../Events');
const { createUser, createMessage, createChat } = require('./Factories');

let connectedUsers = {};
let communityChat = createChat({ isCommunnity: true });

module.exports = function (socket) {
  let sendMessageToChatFromUser;
  let sendTypingFromUser;

  socket.on(VERIFY_USER, (username, cb) => {
    if (isUserExists(username, connectedUsers)) {
      cb({ user: null, isExists: true });
    } else {
      cb({ user: createUser({ name: username, socketId: socket.id }), isExists: false });
    }
  });

  socket.on(USER_CONNECTED, (user) => {
    user.socketId = socket.id;
    connectedUsers = addUser(user, connectedUsers);
    socket.user = user;

    sendMessageToChatFromUser = sendMessageToChat(user.name);
    sendTypingFromUser = sendTypingToChat(user.name);

    io.emit(USER_CONNECTED, connectedUsers);

  });

  // Disconnect
  socket.on('disconnect', () => {
    if ('user' in socket) {
      connectedUsers = removeUser(socket.user.name, connectedUsers);
      io.emit(USER_DISCONNECTED, connectedUsers);
    }
  });

  // LOGOUT
  socket.on(LOGOUT, () => {
    connectedUsers = removeUser(socket.user.name, connectedUsers);
    io.emit(USER_DISCONNECTED, connectedUsers);
  });

  //Get Community Chat
  socket.on(COMMUNITY_CHAT, (callback)=> {
    callback(communityChat);
  });

  socket.on(MESSAGE_SENT, ({ chatId, message }) => {
    sendMessageToChatFromUser(chatId, message);
  });

  socket.on(TYPING, ({ chatId, isTyping }) => {
    sendTypingFromUser(chatId, isTyping);
  });

  socket.on(PRIVATE_MESSAGE, ({ reciever, sender, activeChat }) => {
    if (reciever in connectedUsers) {
      const recieverSocket = connectedUsers[reciever].socketId;
      if (activeChat === null || activeChat.id === communityChat.id) {
        const newChat = createChat({ name: `${reciever}&${sender}`, users: [reciever, sender] });
        socket.to(recieverSocket).emit(PRIVATE_MESSAGE, newChat);
        socket.emit(PRIVATE_MESSAGE, newChat);
      } else {
        socket.to(recieverSocket).emit(PRIVATE_MESSAGE, activeChat);
      }
    }
  });

};

// Internal helper functions
function sendTypingToChat(user) {
  return (chatId, isTyping)=> {
    io.emit(`${TYPING}-${chatId}`, { user, isTyping });
  };
}

function isUserExists(username, userList) {
  return username in userList;
};

function addUser(user, userList) {
  let updateList = Object.assign({}, userList);
  updateList[user.name] = user;
  return updateList;
};

function removeUser(username, userList) {
  let updateList = Object.assign({}, userList);
  delete updateList[username];
  return updateList;
};

function sendMessageToChat(sender) {
  return (chatId, message) => {
    io.emit(`${MESSAGE_RECIEVED}-${chatId}`, createMessage({ sender, message }));
  };
}
