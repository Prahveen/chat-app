const uuid = require('uuid/v4');

const createUser = ({ name = '', socketId = null } = {}) => (
  {
    id: uuid(),
    name,
    socketId,
  }
);

const createMessage = ({ sender = '', message = '' } = {}) => (
  {
    id: uuid(),
    sender,
    message,
  }
);

const createChat = ({ messages = [], name = 'Community', users = [], isCommunnity = false } = {}) => (
  {
    id: uuid(),
    name: isCommunnity ? 'Community' : createChatNameFromUsers(users),
    messages,
    users,
    typingUsers: [],
    isCommunnity,
  }
);

const createChatNameFromUsers = (users, excludeUser = '') => {
  return users.filter(u => u !== excludeUser).join(' & ') || 'Empty Users';
};

module.exports = {
  createUser,
  createMessage,
  createChat,
  createChatNameFromUsers,
};
