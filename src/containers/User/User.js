import React, { Component } from 'react';
import io from 'socket.io-client';
import LoginForm from '../../components/LoginForm/LoginForm';
import Chat from '../../components/Chat/Chat';
import Aux from '../../hoc/Aux';

import { USER_CONNECTED, LOGOUT } from '../../Events';

// Get the IP and Port instead of hardcoding
const SOCKET_URL = 'http://localhost:8888';

class User extends Component {
  constructor(props) {
    super(props);
    this.state = {
      socket: null,
      user: null,
    };
  }

  // Connect to server while loading Layout component
  componentWillMount = () => this.socketInitHandler();

  socketInitHandler = () => {
    const socket = io(SOCKET_URL);
    socket.on('connect', () => {
      console.log('Connected');
    });
    this.setState({ socket: socket });
  };

  setUserHandler = (user) => {
    const socket = this.state.socket;
    socket.emit(USER_CONNECTED, user);
    this.setState({ user: user });
  };

  logOutHandler = () => {
    const socket = this.state.socket;
    socket.emit(LOGOUT);
    this.setState({ user: null });
  };

  render() {
    return (
      <Aux>
        {!this.state.user ?
            <LoginForm
              socket={this.state.socket}
              setUserHandler={this.setUserHandler} />
            :
            <Chat
              socket={this.state.socket}
              user={this.state.user}
              logout={this.logOutHandler}
            />}
      </Aux>
    );
  }
}

export default User;
