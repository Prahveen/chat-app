import React from 'react';

const loginFormError = (props) => (
  <div>
    <p>{props.error}</p>
  </div>
);

export default loginFormError;
