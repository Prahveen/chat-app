import React, { Component } from 'react';
import LoginFormError from './LoginFormError/LoginFormError';
import { VERIFY_USER } from '../../Events';

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      error: null,
    };
  };

  componentWillUnmount = () => this.setErrorHandler('');

  setUserHandler = ({ user, isExists }) => {
    console.log(user, isExists);
    if (isExists) {
      this.setErrorHandler('Username Taken');
    } else {
      this.props.setUserHandler(user);
    }
  };

  submitHandler = (e) => {
    console.log(e);
    e.preventDefault();
    const socket = this.props.socket;
    const username = this.state.username;
    console.log(socket, username);
    socket.emit(VERIFY_USER, username, this.setUserHandler);
  };

  changeHandler = (e) => {
    this.setState({ username: e.target.value });
  };

  setErrorHandler = (error) => {
    this.setState({ error });
  };

  render() {
    return (
      <div>
        <form onSubmit={this.submitHandler}>
          <lable  htmlFor="username">
            <h2> Got a username? </h2>
          </lable>
            <input
              ref={ (input) => this.textInput = input}
              type="text"
              id="username"
              value={this.state.username}
              onChange={this.changeHandler}
              placeholder={'EnterYourName'}
              />
            <div>
              {
                this.state.error ? <LoginFormError  error={this.state.error}/> : null
              }
            </div>
        </form>
      </div>
    );
  }
}

export default LoginForm;
