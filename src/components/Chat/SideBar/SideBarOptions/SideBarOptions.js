import React from 'react';
import PropTypes from 'prop-types';

export const SideBarOptions = (props) => {
    const { active, lastMessage, name, onClick } = props;
    return (
        <div
          className={`user ${active ? 'active' : ''}`}
          onClick={ onClick }
        >
          <div className="user-photo">{name[0].toUpperCase()}</div>
          <div className="user-info">
            <div className="name">{name}</div>
            <div className="last-message">{lastMessage}</div>
          </div>
        </div>
    );
  };

SideBarOptions.PropTypes = {
  name: PropTypes.string.isRequired,
  lastMessage: PropTypes.string,
  active: PropTypes.bool,
  onClick: PropTypes.func,
};

SideBarOptions.defaultProps = {
  lastMessage: '',
  active: false,
  onClick: () => { },
};
