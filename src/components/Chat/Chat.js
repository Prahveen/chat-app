import React, { Component } from 'react';
import SideBar from './SideBar/SideBar';
import ChatHeader from './ChatHeader/ChatHeader';
import Messages from './Messages/Messages';
import MessageInput from './MessageInput/MessageInput';
import { values } from 'lodash';
import { COMMUNITY_CHAT, MESSAGE_SENT, MESSAGE_RECIEVED, TYPING,
  PRIVATE_MESSAGE, USER_CONNECTED, USER_DISCONNECTED } from '../../Events';

class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chats: [],
      users: [],
      activeChat: null,
    };
  }

  componentDidMount() {
    const socket = this.props.socket;
    this.initSocketHanlder(socket);
  }

  componentWillUnMound() {
    const socket = this.props.socket;
    socket.off(PRIVATE_MESSAGE);
    socket.off(USER_CONNECTED);
    socket.off(USER_DISCONNECTED);
  }

  initSocketHanlder = (socket) => {
    socket.emit(COMMUNITY_CHAT, this.resetChatHandler);
    socket.on(PRIVATE_MESSAGE, this.addChatHandler);
    socket.on('connect', () => {
      socket.emit(COMMUNITY_CHAT, this.resetChatHandler);
    });
    //  socket.emit(PRIVATE_MESSAGE, { reciever: 'mike', sender: this.props.user.name });
    socket.on(USER_CONNECTED, (users) => {
      this.setState({ users: values(users) });
    });
    socket.on(USER_DISCONNECTED, (users) => {
      this.setState({ users: values(users) });
    });
  };

  sendOpenPrivateMessage = (reciever) => {
    console.log('sendOpenPrivateMessage', reciever);
    const socket = this.props.socket;
    const activeChat = this.state.activeChat;
    socket.emit(PRIVATE_MESSAGE, { reciever, sender: this.props.user.name, activeChat });
  };

  activeChatHandler = (activeChat) => {
    this.setState({ activeChat });
  };

  sendMessageHandler = (chatId, message) => {
    console.log('sendMessageHandler', chatId, message);
    const socket = this.props.socket;
    socket.emit(MESSAGE_SENT, { chatId, message });

  };

  sendTypingHandler = (chatId, isTyping) => {
    const socket = this.props.socket;
    socket.emit(TYPING, { chatId, isTyping });
  };

  resetChatHandler = (chat) => this.addChatHandler(chat, true);

  addChatHandler = (chat, reset = false) => {
    const socket = this.props.socket;
    const chats = this.state.chats;
    const newChats = reset ? [chat] : [...chats, chat];
    this.setState({ chats: newChats, activeChat: reset ? chat : this.state.activeChat });

    const messageEvent = `${MESSAGE_RECIEVED}-${chat.id}`;
    const typingEvent = `${TYPING}-${chat.id}`;

    socket.on(typingEvent, this.updateChatTypingChat(chat.id));
    socket.on(messageEvent, this.addMessageToChat(chat.id));
  };

  addMessageToChat = (chatId) => {
    return message => {
      const chats = this.state.chats;
      let newChats = chats.map((chat) => {
        if (chat.id === chatId)
          chat.messages.push(message);
        return chat;
      });
      this.setState({ chats: newChats });
    };
  };

  updateChatTypingChat = (chatId) => {
    return ({ isTyping, user })=> {
      if (user !== this.props.user.name) {

        let newChats = this.state.chats.map((chat)=> {
          if (chat.id === chatId) {
            if (isTyping && !chat.typingUsers.includes(user)) {
              chat.typingUsers.push(user);
            }else if (!isTyping && chat.typingUsers.includes(user)) {
              chat.typingUsers = chat.typingUsers.filter(u => u !== user);
            }
          }

          return chat;
        });
        this.setState({ chats: newChats });
      }
    };
  };

  render() {
    return (
      <div className="container">
        <SideBar
          user={this.props.user}
          users={this.state.users}
          chats={this.state.chats}
          activeChat={this.state.activeChat}
          setActiveChat={this.activeChatHandler}
          logout= {this.props.logout}
          onSendPrivateMessage = {this.sendOpenPrivateMessage} />
          <div className="chat-room-container">
          {
            this.state.activeChat !== null ? (
              <div className="chat-room">
                <ChatHeader name={this.state.activeChat.name} />
                <Messages
                  messages={this.state.activeChat.messages}
                  user={this.props.user}
                  typingUsers={this.state.activeChat.typingUsers}
                />
                <MessageInput
                  sendMessage={
                    (message)=> {
                      this.sendMessageHandler(this.state.activeChat.id, message);
                    }
                  }
                  sendTyping={
                    (isTyping)=> {
                      this.sendTypingHandler(this.state.activeChat.id, isTyping);
                    }
                  }/>
              </div>)
            :
            <div className="chat-room choose">
              <h3>Choose a chat!</h3>
            </div>
          }
  				</div>
      </div>
    );
  }

};

export default Chat;
