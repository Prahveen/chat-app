import React, { Component } from 'react';
import Layout from './components/Layout/Layout';
import User from './containers/User/User';
import './index.css';

class App extends Component {
  render() {
    return (
      <Layout>
        <User/>
      </Layout>
    );
  }
}

export default App;
