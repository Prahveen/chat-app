#Simple Chat Application

## Instruction for start application

### * Run install
    ```
      $ npm install
    ```
### * Run server in one shell
    ```
      $ npm run server
    ```
### * Run client in another shell
    ```
      $ npm run react
    ```
### Open your browser and open two tabs, 
  default port for runing client is port 3000. Type in your address bar [http://localhost:3000](http://localhost:3000)  for open client interface.

### * Login
 Enter your name and press enter
 
 ![Loging](https://drive.google.com/uc?id=1PkNVpPMushqR68NVG15bD8NRRKT7pR4J)
 
### * Get Online users
  Click Users in left side bar
  
  ![Online Users](https://drive.google.com/uc?id=1kcvc0aUQ0QsxlHEeyXmPy6hQE_u9CwkT)

### * Overall UI discription

  ![Overall description](https://drive.google.com/uc?id=17qY04UD_Xc0m3VmY_JRQYLotCuIf76yM)